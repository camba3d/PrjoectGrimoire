﻿Shader "Custom/TraslucentWall" {
	Properties {
		_Color ("Emission Color", Color) = (1,1,1,1)
		_MainTex ("Emission Texture (BW)", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "white" {}
		_EmissionAmount ("Emission Amount", Range(0,10)) = 1
		_Glossiness("Glossiness", Range(0,1)) = 0.5
		_Darkness("Shade Amount", Range(0,1)) = 0.5
	}
	SubShader {
		Tags { "Queue"="Overlay" "RenderType"="Transparent"  }
		LOD 200

		Zwrite off
		// 1a pasada: renderiza su background como textura
		GrabPass {}

		// 2a pasada: Aplicamos la textura renderizada en la 1a y le añadimos efectos y otras texturas
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		sampler2D _GrabTexture;	// Textura renderizada a partir de lo que tiene detrás
		float4 _Color;
		float _EmissionAmount;
		float _Glossiness;
		float _Darkness;

		struct Input {
			float2 uv_MainTex;
			float4 screenPos;
		};

		UNITY_INSTANCING_BUFFER_START(Props)
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {

			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
			o.Albedo = (tex2Dproj(_GrabTexture, IN.screenPos)).rgb *(1-_Darkness);
			o.Emission = tex2D(_MainTex, IN.uv_MainTex) * _Color * _EmissionAmount;
			o.Smoothness = _Glossiness;
			o.Alpha = 1.0;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
