Shader "Custom/MagicCrystal" {
	Properties {
		_MainTex ("Texture (RGB)", 2D) = "white" {}
		_MainNormalMap("Normal Map", 2D) = "white" {}
		_CrackTex("Cracks", 2D) = "white" {}
		_CrackNorm("Cracks Normal Map", 2D) = "white" {}
		_CrackVisibility("Cracks visibility", Range(0,1)) = 0.0
		_NumCracks("Number of Cracks", Int) = 0
		_Color ("Emission color", Color) = (1,1,1,1)
		_EmissionAmount ("Emission Amount", Range(0,2)) = 0.75
		_Glossiness ("Glossy", Range(0,1)) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		//Zwrite off

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MainNormalMap;
		sampler2D _CrackTex;
		sampler2D _CrackNorm;
		fixed _CrackVisibility;
		float _EmissionAmount;
		int _NumCracks;

		struct Input {
			float2 uv_MainTex;
			float2 uv_MainNormalMap;
			float2 uv_CrackTex;
			float4 screenPos;
			float3 viewDir;
			//float3 N : NORMAL;
		};

		half _Glossiness;
		fixed4 _Color;

		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {	

			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		    float3 N = UnpackNormal(tex2D(_MainNormalMap, IN.uv_MainTex));
            //float3 N = o.Normal;

			float anguloVision = dot(IN.viewDir, N);
			o.Emission = _Color * _EmissionAmount * ( anguloVision*0.5 + tex2D(_MainTex, IN.uv_MainTex));
			
			for (int i=1; i<_NumCracks+1; i++) {
				float2 newUV = IN.uv_CrackTex;
				newUV *= i;
				//distortAmount += tex2D(_CrackTex, newUV).xyz * _CrackVisibility;
				c += tex2D(_CrackTex, newUV) * 1.5 * _CrackVisibility;
				N += UnpackNormal(tex2D(_CrackNorm, newUV));
			}

			o.Albedo = c.rgb;
            o.Normal = N;
            o.Smoothness = _Glossiness;
		}
		ENDCG
	}
	FallBack "Diffuse"
}