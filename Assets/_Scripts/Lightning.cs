// Code based on LightningBoltUnity.cs by @TrevorKinsie on Github
using UnityEngine;
using System.Collections;

public class Lightning : MonoBehaviour {

    private LineRenderer lRend;
    private Vector3[] points = new Vector3[5];

    private readonly int ORIGIN_POINT    = 0;
    private readonly int MIDDLE_POINT_L  = 1;
    private readonly int MIDDLE_POINT    = 2;
    private readonly int MIDDLE_POINT_R  = 3;
    private readonly int ENDING_POINT    = 4;

    public float randomPosOffset = 0.3f;
    private readonly float randomWithOffsetMax = 5f;
    private readonly float randomWithOffsetMin = 0.5f;

    private readonly WaitForSeconds customFrame = new WaitForSeconds(0.05f);

    void Start () {
        lRend = GetComponent<LineRenderer>();
        StartCoroutine(Beam());

        points[ORIGIN_POINT] = transform.GetChild(1).gameObject.transform.position;
	    //Debug.LogWarning(points[ORIGIN_POINT]);        
        points[ENDING_POINT] = transform.GetChild(0).gameObject.transform.position;
	    //Debug.LogWarning(points[ENDING_POINT]);        
	}

    private IEnumerator Beam()
    {
        yield return customFrame;
        
        points[ORIGIN_POINT] = transform.GetChild(1).gameObject.transform.position;
        points[ENDING_POINT] = transform.GetChild(0).gameObject.transform.position;

        CalculateMiddle();
        lRend.SetPositions(points);
        lRend.SetWidth(RandomWidthOffset(), RandomWidthOffset());
        StartCoroutine(Beam());
    }

    private float RandomWidthOffset()
    {
        return Random.Range(randomWithOffsetMin, randomWithOffsetMax);
    }

    private void CalculateMiddle()
    {
        Vector3 center = GetMiddleWithRandomness(points[ORIGIN_POINT], points[ENDING_POINT]);

        points[MIDDLE_POINT] = center;
        points[MIDDLE_POINT_L] = GetMiddleWithRandomness(points[ORIGIN_POINT], points[MIDDLE_POINT]);
        points[MIDDLE_POINT_R] = GetMiddleWithRandomness(points[MIDDLE_POINT], points[ENDING_POINT]);
    }

    private Vector3 GetMiddleWithRandomness (Vector3 point1, Vector3 point2)
    {
        float x = (point1.x + point2.x) / MIDDLE_POINT;
        float finalX = Random.Range(x - randomPosOffset, x + randomPosOffset);
        float y = (point1.y + point2.y) / MIDDLE_POINT;
        float finalY = Random.Range(y - randomPosOffset, y + randomPosOffset); 
        float z = (point1.z + point2.z) / MIDDLE_POINT;
        float finalZ = Random.Range(z - randomPosOffset, z + randomPosOffset); 

        return new Vector3(finalX, finalY, finalZ);
    }
}