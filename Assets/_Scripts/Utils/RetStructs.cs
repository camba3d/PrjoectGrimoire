﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RetStructs
{
    public struct AttackPos
    {
        public bool areFreePos;
        public Vector3 attackPos;
    };
}
