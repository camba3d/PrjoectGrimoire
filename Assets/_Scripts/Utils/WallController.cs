﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour {

	// Use this for initialization
	private const string  TAG = "MainCamera";

	[SerializeField]
	private Material transparentWall;
	[SerializeField]
	private Material startMaterial;
	[SerializeField]
	private GameObject[] go;

	private Material m;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void OnTriggerEnter(Collider other) {


		if (string.Compare (other.tag, TAG) == 0) {
			Debug.LogWarning ("Entro en el trigger");
			for (int i = 0; i < go.Length; i++) {
				go[i].GetComponent<Renderer> ().material = transparentWall;
			}
		}

		
	}

	void OnTriggerExit(Collider other) {


		if (string.Compare (other.tag, TAG) == 0) {
			Debug.LogWarning ("Salgo del trigger");
			for (int i = 0; i < go.Length; i++) {
				go[i].GetComponent<Renderer> ().material = startMaterial;
			}
		}


	}

	 
}
