﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerMenu : MonoBehaviour {

	// Use this for initialization

	private const string NUEVA_PARTIDA 		= "NuevaPartida";
	private const string CREDITOS 		= "Creditos";
	private static ControllerMenu instance;
	public static ControllerMenu GetInstance(){
		return instance;
	}

	[SerializeField] private Animator animSnake;
	[SerializeField] private Animator animHead;
	[SerializeField] private Animator animPig;
	[SerializeField] private Animator creditsPanel;
	[SerializeField] private AudioSource aSource;

	bool show=false;

	void Start () {

		instance = this;
//		InvokeRepeating ("EnemyAnimation", 2f,2f);
		StartCoroutine("Anim");
		StartCoroutine ("PlaySound");
	}
	
	// Update is called once per frame
	void Update () {
		
	}




	public void AccionBoton(string nombreBoton){
		Debug.LogWarning ("GESTOR DE BOTONES");

		switch (nombreBoton) {
		case NUEVA_PARTIDA:
			Application.LoadLevel(1);
			break;
		case CREDITOS:
			Debug.LogWarning ("Creditos");
			if (show == false) {
				Debug.LogWarning ("MostrarCreditos");

				creditsPanel.SetBool ("Show", true);
				StartCoroutine ("ShowCredits",true);			
			}
			if (show == true) {
				Debug.LogWarning ("no mostrar");

				creditsPanel.SetBool ("Show", false);
				StartCoroutine ("ShowCredits",false);			

			}

			break;
		}

	}

	IEnumerator ShowCredits (bool value){
		yield return new WaitForSeconds (0.5f);
		show = value;
	}

	IEnumerator PlaySound (){
		yield return new WaitForSeconds (0.5f);
		aSource.Play ();
	}
	void EnemyAnimation (){
		StartCoroutine ("Anim");
	}
	IEnumerator Anim (){
	 
		yield return new WaitForSeconds (4f);
		animSnake.Play("asomar");
		yield return new WaitForSeconds (3f);
		animHead.Play("asomar");
		yield return new WaitForSeconds (4f);
		animPig.Play ("asomar");
//		Debug.Log ("Pruebaaaaa");
//		int num = Random.Range (0, 4);
//		Debug.LogWarning ("Me han invocado el enemigo numero" + num);
//		switch (num) {
//		case 1: //Snake
//			//animSnake.SetBool ("asomar", true);
//			animSnake.Play("asomar");
//			break;
//		case 2: 
//			//animHead.SetBool ("asomar", true);
//			animHead.Play("asomar");
//			break;
//	
//		case 3:
//			animPig.Play ("asomar");
//			break;
//
//		}
	
	}

}
