﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World2Walk : MonoBehaviour
{
    // Use this for initialization
    [Header("GameObjects")]
    [SerializeField]
    private GameObject enemy; // enemigo a invocar

    [Header("Parametres")]
    [SerializeField] private float fwSpeed = 3;
    [SerializeField] private int time = 3; //cada cuento se invoca un enemigo
    [SerializeField] private int numEnemy = 10; //cuantos enemigos se invocan en total

    private int count; // count enemy create
    private List<GameObject> enemyInst = new List<GameObject>();

    void Start()
    {
        InvokeRepeating("CreateEnemy", 2, time);
    }

    void Update()
    {

        for (int i = 0; i < enemyInst.Count; i++)
        {
            GameObject go = enemyInst[i];
            if (go != null)
                go.transform.position += go.transform.forward * fwSpeed * Time.deltaTime;
            else
                enemyInst.Remove(go);
        }

        if (count == numEnemy)
            CancelInvoke("CreateEnemy");
    }

    /// <summary>
    /// Creates the enemy.
    /// </summary>
    private void CreateEnemy()
    {
        Vector3 instPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 3);
        enemyInst.Add(GameObject.Instantiate(enemy, instPos, Quaternion.identity));
        count++;
    }
}
