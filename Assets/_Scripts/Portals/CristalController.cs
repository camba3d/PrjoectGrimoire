﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CristalController : MonoBehaviour
{

    // Use this for initialization

    private const string TAG = "Sword";
    private int count;
    private int numcraks = 2;
    private GameObject cristal;
    [SerializeField]
    private GameObject cristalbreak;

	[SerializeField]
	private GameObject lightray;
	[SerializeField]
	private AudioSource cristalHit;

	bool desactivatePortal=true;
    private int crakstouchs = 1;
    private int brokentouch = 2;
    private int explosiontouch = 3;

	public PortalController portalController;
    void Start()
    {
        cristal = this.transform.GetChild(3).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (count == crakstouchs && cristal.GetComponent<Renderer>())
        {
            cristal.GetComponent<Renderer>().material.SetInt("_NumCracks", numcraks);
        }

        if (count == brokentouch)
        {
            cristal.SetActive(false);
            cristalbreak.SetActive(true);
        }
        if (count == explosiontouch)
        {
            cristalbreak.SetActive(false);
            StartCoroutine("SplitMesh", true);
            count++;

			// desactivamos el rayo
			if (desactivatePortal = true) {
				desactivatePortal = false;
				lightray.SetActive (false);
				portalController.numCristals--;
			}



        }

    }

    void OnTriggerEnter(Collider other)
    {
        //Debug.LogWarning("Entro en el trigger:" + other.name);


		if (string.Compare(other.tag, TAG) == 0 && PlayerController.instance.attack)
        {
            //Debug.LogWarning("Entro en el trigger");
			//AudioManager.instance.Play("cristal_hit");
			cristalHit.Play();
            count++;
        }
    }



    /// <summary>
    /// Splits the mesh.
    /// </summary>
    /// <returns>The mesh.</returns>
    /// <param name="destroy">If set to <c>true</c> destroy.</param>
    public IEnumerator SplitMesh(bool destroy)
    {

        if (cristalbreak.GetComponent<MeshFilter>() == null || cristalbreak.GetComponent<SkinnedMeshRenderer>() == null)
        {
            yield return null;
        }

        if (cristalbreak.GetComponent<Collider>())
        {
            cristalbreak.GetComponent<Collider>().enabled = false;
        }

        Mesh M = new Mesh();
        if (cristalbreak.GetComponent<MeshFilter>())
        {
            M = cristalbreak.GetComponent<MeshFilter>().mesh;
        }
        else if (cristalbreak.GetComponent<SkinnedMeshRenderer>())
        {
            M = cristalbreak.GetComponent<SkinnedMeshRenderer>().sharedMesh;
        }

        Material[] materials = new Material[0];
        if (cristalbreak.GetComponent<MeshRenderer>())
        {
            materials = cristalbreak.GetComponent<MeshRenderer>().materials;
        }
        else if (cristalbreak.GetComponent<SkinnedMeshRenderer>())
        {
            materials = cristalbreak.GetComponent<SkinnedMeshRenderer>().materials;
        }

        Vector3[] verts = M.vertices;
        Vector3[] normals = M.normals;
        Vector2[] uvs = M.uv;

        int loopJumps = (int)(Mathf.Floor(M.subMeshCount / 9));
        for (int submesh = 0; submesh < M.subMeshCount - loopJumps; submesh++)
        {

            //Debug.LogWarning("Numero de veerrtices: " + M.vertices.Length);

            int[] indices = M.GetTriangles(submesh);

            for (int i = 0; i < indices.Length; i += 3)
            {
                Vector3[] newVerts = new Vector3[3];
                Vector3[] newNormals = new Vector3[3];
                Vector2[] newUvs = new Vector2[3];
                for (int n = 0; n < 3; n++)
                {
                    int index = indices[i + n];
                    newVerts[n] = verts[index];
                    newUvs[n] = uvs[index];
                    newNormals[n] = normals[index];
                }

                Mesh mesh = new Mesh
                {
                    vertices = newVerts,
                    normals = newNormals,
                    uv = newUvs,

                    triangles = new int[] { 0, 1, 2, 2, 1, 0 }
                };

                GameObject GO = new GameObject("Triangle " + (i / 3))
                {
                    //layer = LayerMask.NameToLayer("Particle")
                };
                GO.transform.position = cristalbreak.transform.position;
                GO.transform.rotation = cristalbreak.transform.rotation;
                GO.transform.localScale = cristalbreak.transform.localScale;
                GO.AddComponent<MeshRenderer>().material = materials[submesh];
                GO.AddComponent<MeshFilter>().mesh = mesh;
                GO.AddComponent<BoxCollider>();
                Vector3 explosionPos = new Vector3(cristalbreak.transform.position.x + Random.Range(-0.5f, 0.5f), cristalbreak.transform.position.y + Random.Range(0f, 0.5f), cristalbreak.transform.position.z + Random.Range(-0.5f, 0.5f));
                GO.AddComponent<Rigidbody>().AddExplosionForce(Random.Range(300, 500), explosionPos, 5);
                Destroy(GO, Random.Range(0.1f, 0.9f));
            }
        }

        cristalbreak.GetComponent<Renderer>().enabled = false;

        yield return new WaitForSeconds(5.0f);
        if (destroy == true)
        {
            Destroy(cristalbreak);
        }

    }

}
