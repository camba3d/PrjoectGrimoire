﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistArea : MonoBehaviour
{
    #region Public Vars
    public static DistArea instance;
    #endregion Public Vars

    #region MonoBehaviour Methods
    void Awake()
    {
        // Singleton
        if (instance == null) { instance = this; }
        else { Destroy(gameObject); return; }
    }
    #endregion MonoBehaviour Methods

    #region Other Methods
    public RetStructs.AttackPos GetAttackPos()
    {
        bool areFreePos = false;
        Vector3 attackPos = Vector3.zero;

        for (var i = 0; i < this.transform.childCount; i++)
        {
            var c = transform.GetChild(i);
            if ((c.GetComponent("QuadCounter") as QuadCounter).isFree)
            {
                areFreePos = true;
                attackPos = c.position;
                break;
            }
        }

        RetStructs.AttackPos ap = new RetStructs.AttackPos
        {
            areFreePos = areFreePos,
            attackPos = attackPos
        };
        return ap;
    }
    #endregion Other Methods
}
