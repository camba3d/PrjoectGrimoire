﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager instance;

    int nDistAttackers = 0;
    int nMeleeAttackers = 0;
    const int maxDistAttackers = 2;
    const int maxMeleeAttackers = 1;

    // Use this for initialization
    void Awake()
    {
        // Singleton
        if (instance == null) { instance = this; }
        else { Destroy(gameObject); return; }
    }

    // Update is called once per frame
    public bool IsAbusive(Enemy.AttackTypes at)
    {
        if (at == Enemy.AttackTypes.Distance)
            return nDistAttackers >= maxDistAttackers;
        else
            return nMeleeAttackers >= maxMeleeAttackers;
    }
    internal void IncAttackers(Enemy.AttackTypes at)
    {
        if (at == Enemy.AttackTypes.Distance) nDistAttackers++;
        else nMeleeAttackers++;
    }
    internal void DecAttackers(Enemy.AttackTypes at)
    {
        if (at == Enemy.AttackTypes.Distance) nDistAttackers--;
        else nMeleeAttackers--;
    }

}
