﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public GameObject[] portals;
	public GameObject[] enemys;
	public GameObject textNextRound;
	public Transform positionBoss;
	public int numPortals=0;
	public bool round=true;
	public int ronda=0;
	public bool tocaBoss= false ;
	public bool tocapasar=false; //cuando matemos al boss se pone en true
	public PlayerController player;
	public int numRondasTotales = 0;
	public GameObject terreno;
	// Use this for initialization
	private static GameManager gameManager;
	private const string PATH_PREFABS = "Prefabs/";


	public static GameManager instance
	{
		get
		{
			if (!gameManager)
			{
				gameManager = FindObjectOfType(typeof(GameManager)) as GameManager;
				if (!gameManager)
					Debug.LogError("There needs to be one active EventManager script on a GameObject in your scene.");
			}
			return gameManager;
		}
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (numPortals == portals.Length && round == true) {
		
			if (tocaBoss == true && ronda==numRondasTotales) {
				//tiramos el boss y activamos el suelo
				Debug.LogWarning ("Tiramos Boss");
				Boss();
				tocaBoss =false; // cuando muera el boss poner a true

			} else if (tocapasar==true){
				tocapasar = false;
				Debug.LogWarning ("PASAMOS AL SIGUEINTE NIVEL");
				round = false;
				textNextRound.SetActive (true);
				// se pasa de ronda
				ronda ++;
				StartCoroutine("ActivarPortales");
			}



		}




	}

	private void Boss()
	{
		Debug.LogWarning("---Path:" + PATH_PREFABS);

		//GameObject go;
		//go = GameObject.Instantiate(Resources.Load<GameObject>(PATH_PREFABS + "Enemy"));
		//go.name = "Enemy";
		//go.transform.position = transform.position;

		//go.transform.SetParent(null);
		//go.GetComponent<EnemyNav>().target = player;



		Vector3 floorPos = GameObject.FindWithTag("Floor").transform.position;
		Vector3 instPos = positionBoss.position;

		GameObject go2;
		int i = Random.Range (0, 8);
		go2 = GameObject.Instantiate(enemys[i], instPos, Quaternion.identity);
		//go2.GetComponent<Enemy>().target = player;
		go2.name = enemys[i].name;
		go2.transform.scaleTo(0.05f,new Vector3 (5.0f,5.0f,5.0f));
		go2.AddComponent<Rigidbody> ();
		go2.GetComponent<Rigidbody>().constraints= RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
		go2.GetComponent<Enemy> ().isBoss = true;
		//UltimateController.instance.SaveEnemy(go2);
		Debug.LogWarning("----------- CREATING ENEMY ----------------");
	}

	#region coroutines

	//
	// Coroutines
	//


	IEnumerator ActivarPortales()
	{
		yield return new WaitForSeconds (4.0f);

			// abrir portales e incrementar enemigos 
			//Actualizo enemigos 
		textNextRound.SetActive(false);
			for (int i = 0; i < enemys.Length; i++) {
				Debug.LogWarning ("i de enemigos " + i);
				enemys [i].GetComponent<Enemy> ().hurt++;
				enemys [i].GetComponent<Enemy> ().Totallife += 2;
			}

			//Actualizo portales 
			for (int i = 0; i < portals.Length; i++) {
				Debug.LogWarning ("i de POTALESSSS " + i);

				portals [i].transform.GetChild (0).GetComponent<InvokeEnemys> ().numEnemy += Random.Range (1, 3);// subida de enemigos aleatoria
				portals [i].transform.GetChild (0).GetComponent<InvokeEnemys> ().count = 0;


				portals [i].transform.GetChild (0).GetComponent<InvokeEnemys> ().enabled = true;

				portals [i].GetComponent<PortalController> ().numCristals = 2;
				portals [i].GetComponent<PortalController> ().desactivate = true;
				portals [i].GetComponent<PortalController> ().ActivatePortal ();

			}



		

	}
	#endregion
}
